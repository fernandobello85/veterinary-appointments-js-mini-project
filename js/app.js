let DB;

const form = document.querySelector('form'),
	petName = document.querySelector('#pet'),
	clientName = document.querySelector('#client'),
	phone = document.querySelector('#phone'),
	date = document.querySelector('#date'),
	hour = document.querySelector('#hour'),
	symptoms = document.querySelector('#symptoms'),
	appointments = document.querySelector('#appointments'),
	headingSchedule = document.querySelector('#schedule');

document.addEventListener('DOMContentLoaded', () => {
	let createDB = window.indexedDB.open('appointments', 1);

	createDB.onerror = function() {
		// console.log('Error occurred');
	}
	createDB.onsuccess = function() {
		// console.log('Ready')
		DB = createDB.result;
		showAppointments()
		// console.log(DB);
	}
	createDB.onupgradeneeded = function(e) {
		let db = e.target.result;
		// key path is the Index DB
		let objectStore = db.createObjectStore('appointments', { keyPath: 'key', autoIncrement: true });
		// create DB indexes and fields
		objectStore.createIndex('pet', 'pet', { unique : false } )
		objectStore.createIndex('client', 'client', { unique : false } )
		objectStore.createIndex('phone', 'phone', { unique : false } )
		objectStore.createIndex('date', 'date', { unique : false } )
		objectStore.createIndex('hour', 'hour', { unique : false } )
		objectStore.createIndex('symptoms', 'symptoms', { unique : false } )
		// console.log('db created')
	}

	form.addEventListener('submit', addData)

	function addData(e) {
		e.preventDefault()
		const newAppointment = {
				pet: petName.value,
				client: clientName.value,
				phone: phone.value,
				date: date.value,
				hour: hour.value,
				symptoms: symptoms.value
		}

		let transaction = DB.transaction(['appointments'], 'readwrite')
		let objectStore = transaction.objectStore('appointments')
		let request = objectStore.add(newAppointment)
		request.onsuccess = () => {
				form.reset()
		}
		transaction.oncomplete = () => {
				showAppointments()
		}
		transaction.onerror = () => {
				// console.log('Error occurred')
		}
	}

	function showAppointments() {
		while(appointments.firstChild) {
				appointments.removeChild(appointments.firstChild)
		}
		let objectStore = DB.transaction('appointments').objectStore('appointments')
		objectStore.openCursor().onsuccess = function(e) {
			let cursor = e.target.result

			if(cursor) {
				let appointmentHTML = document.createElement('li')
				appointmentHTML.setAttribute('data-appointment-id', cursor.value.key)
				appointmentHTML.classList.add('list-group-item')

				appointmentHTML.innerHTML = `
						<p class="font-weight-bold">Pet: <span class="font-weight-normal">${cursor.value.pet}</span></p>
						<p class="font-weight-bold">Client: <span class="font-weight-normal">${cursor.value.client}</span></p>
						<p class="font-weight-bold">Phone: <span class="font-weight-normal">${cursor.value.phone}</span></p>
						<p class="font-weight-bold">Date: <span class="font-weight-normal">${cursor.value.date}</span></p>
						<p class="font-weight-bold">Hour: <span class="font-weight-normal">${cursor.value.hour}</span></p>
						<p class="font-weight-bold">Symptoms: <span class="font-weight-normal">${cursor.value.symptoms}</span></p>
				`
				const removeButton = document.createElement('button')
				removeButton.classList.add('remove', 'btn', 'btn-danger')
				removeButton.innerHTML = '<span aria-hidden="true">x</span> Remove'
				removeButton.onclick = removeAppointment
				appointmentHTML.appendChild(removeButton)

				appointments.appendChild(appointmentHTML)
				cursor.continue()
			} else {
				if(!appointments.firstChild) {
					headingSchedule.textContent = 'Add appointments to begin'
					let appointmentsList = document.createElement('p')
					appointmentsList.textContent = 'No appointments'
					appointments.appendChild(appointmentsList)
					} else {
						headingSchedule.textContent = 'Schedule your pet appointments'
				}
			}
		}
	}

	function removeAppointment(e) {
		let appointmentID = Number(e.target.parentElement.getAttribute('data-appointment-id'))
		let transaction = DB.transaction(['appointments'], 'readwrite')
		let objectStore = transaction.objectStore('appointments')
		let request = objectStore.delete(appointmentID)

		transaction.oncomplete = () => {
			e.target.parentElement.parentElement.removeChild(e.target.parentElement)
			if(!appointments.firstChild) {
				headingSchedule.textContent = 'Add appointments to begin'
				let appointmentsList = document.createElement('p')
				appointmentsList.textContent = 'No appointments'
				appointments.appendChild(appointmentsList)
			} else {
				headingSchedule.textContent = 'Schedule your pet appointments'
			}
		}
	}
})